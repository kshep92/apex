package com.reviselabs.apex.routing;

import com.reviselabs.apex.contracts.RequestHandler;
import com.reviselabs.apex.di.DependencyManager;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

import java.util.Arrays;

/**
 * Author: Kevin Sheppard <kevin@reviselabs.com>
 * Created: 04/03/2017
 *
 * Base class that provides all the shortcut routing methods.
 */
public abstract class RoutingComponent {

  private Router router;

  public Router getRouter() {
    return router;
  }

  public void setRouter(Router router) {
    this.router = router;
  }

  /**
   * Internal method for creating the actual {@link io.vertx.ext.web.Route}.
   * @param method The {@link HttpMethod} to listen for
   * @param url The URL to match
   * @param handler A {@link RequestHandler}
   * @return this instance
   */
  private RoutingComponent doRoute(HttpMethod method, String url, RequestHandler handler) {
    router.route(method, url).handler(new ApexRequestHandler(handler));
    return this;
  }

  /**
   * Takes a {@link RoutingContext} and converts it into an {@link ApexRoutingContext}.
   * @param context the vanilla {@link RoutingContext}
   * @return this instance
   */
  public ApexRoutingContext createContext(RoutingContext context) {
    @SuppressWarnings("ConstantConditions")
    ApexRoutingContext apexRoutingContext = DependencyManager.getInjector().getInstance(ApexRoutingContext.class);
    apexRoutingContext.setContext(context);
    return apexRoutingContext;
  }

  /**
   * Attach a single handler to a GET URL.
   * @param url the URL to match
   * @param handler A {@link RequestHandler}
   */
  public void get(String url, RequestHandler handler) {
    doRoute(HttpMethod.GET, url, handler);
  }

  /**
   * Attach several handlers to a GET URL.
   * @param url the URL to match
   * @param handlers A {@link RequestHandler}
   */
  public void get(String url, RequestHandler... handlers) {
    Arrays.asList(handlers).forEach(handler -> get(url, handler));
  }

  /**
   * Attach a handler to POST URL.
   * @param url the URL to match
   * @param handler A {@link RequestHandler}
   */
  public void post(String url, RequestHandler handler) {
    doRoute(HttpMethod.POST, url, handler);
  }

  /**
   * Attach several handlers to a POST URL.
   * @param url the URL to match
   * @param handlers A {@link RequestHandler}
   */
  public void post(String url, RequestHandler... handlers) {
    Arrays.asList(handlers).forEach(handler -> post(url, handler));
  }

  /**
   * Attach a handler to a PUT URL.
   * @param url the URL to match
   * @param handler A {@link RequestHandler}
   */
  public void put(String url, RequestHandler handler) {
    doRoute(HttpMethod.PUT, url, handler);
  }

  /**
   * Attach several handlers to a PUT URL.
   * @param url the URL to match
   * @param handlers A {@link RequestHandler}
   */
  public void put(String url, RequestHandler... handlers) {
    Arrays.asList(handlers).forEach(handler -> put(url, handler));
  }

  /**
   * Attach a handler to a DELETE URL.
   * @param url the URL to match
   * @param handler A {@link RequestHandler}
   */
  public void delete(String url, RequestHandler handler) {
    doRoute(HttpMethod.DELETE, url, handler);
  }

  /**
   * Attach several handlers to a DELETE URL.
   * @param url the URL to match
   * @param handlers A {@link RequestHandler}
   */
  public void delete(String url, RequestHandler... handlers) {
    Arrays.asList(handlers).forEach(handler -> delete(url, handler));
  }

  /**
   * Add a Before Filter to a path.
   * @param url The URL to match
   * @param handler A {@link RequestHandler}
   */
  public void before(String url, RequestHandler handler) {
    router.route(url).handler(new ApexRequestHandler(handler));
  }

  /**
   * Add a Before Filter to all paths.
   * @param handler A {@link RequestHandler}
   */
  public void before(RequestHandler handler) {
    router.route().handler(new ApexRequestHandler(handler));
  }

  /**
   * Used for wrapping a vanilla Vert.x route with our custom ApexRequestHandler
   * @param route A Vert.x route created with {@link Router#route}
   * @param handler A {@link RequestHandler}
   */
  public void when(Route route, RequestHandler handler) {
    route.handler(new ApexRequestHandler(handler));
  }

  private class ApexRequestHandler implements Handler<RoutingContext> {
    private RequestHandler handler;

    protected ApexRequestHandler(RequestHandler handler) {
      this.handler = handler;
    }

    @Override
    public void handle(RoutingContext context) {
      ApexRoutingContext apexRoutingContext = DependencyManager.getInjector().getInstance(ApexRoutingContext.class);
      apexRoutingContext.setContext(context);
      handler.handle(apexRoutingContext);
    }
  }
}
