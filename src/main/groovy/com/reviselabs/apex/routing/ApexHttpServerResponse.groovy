package com.reviselabs.apex.routing
import com.fasterxml.jackson.databind.ObjectMapper
import com.reviselabs.apex.config.Environment
import io.vertx.core.http.HttpServerResponse
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.templ.TemplateEngine
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static java.net.HttpURLConnection.*
/**
 * Author: Kevin Sheppard <kevin@reviselabs.com>
 * Created: 3/4/2017
 *
 * Wrapper class for {@link HttpServerResponse}
 */
class ApexHttpServerResponse {
  @Delegate
  HttpServerResponse response
  RoutingContext context
  TemplateEngine templateEngine
  String templatesPath
  Logger logger

  protected ApexHttpServerResponse(RoutingContext context, HttpServerResponse response, TemplateEngine templateEngine, String templatesPath) {
    this.response = response
    this.templateEngine = templateEngine
    this.templatesPath = templatesPath
    this.context = context
    logger = LoggerFactory.getLogger(getClass())
  }

  /**
   * Sets the response code to 400.
   * @return
   */
  ApexHttpServerResponse badRequest() {
    setStatusCode(HTTP_BAD_REQUEST);
    return this
  }

  void badRequest(String message) {
    badRequest().text(message)
  }

  /**
   * Sets the response code to 500.
   * @return
   */
  ApexHttpServerResponse error() {
    setStatusCode(HTTP_INTERNAL_ERROR)
    return this
  }

  void error(String message) {
    error().text(message)
  }

  /**
   * Sets the response code to 403.
   * @return
   */
  ApexHttpServerResponse forbidden() {
    setStatusCode(HTTP_FORBIDDEN);
    return this
  }

  void forbidden(String message) {
    forbidden().text(message)
  }

  void html(String html) {
    putHeader("content-type", "text/html").end(html)
  }

  ApexHttpServerResponse notFound() {
    setStatusCode(HTTP_NOT_FOUND)
    return this
  }

  void notFound(String message) {
    notFound().text(message)
  }
  /**
   * Render a view template.
   * @param template The path to the template file.
   * @param data A {@link Map} of data to be passed to the template.
   */
  void render(String templateName, Map data = [:]) {
    data.forEach({ String k, Object v -> context.put(k, v) })
    if (templateEngine) {
      templateEngine.render(context, "$templatesPath/$templateName", { result ->
        putHeader("content-type", "text/html");
        if (result.succeeded()) {
          putHeader("content-length", String.valueOf(result.result().length()))
              .write(result.result())
              .end();
        } else {
          error()
          StringBuilder message = new StringBuilder();
          message.append('<div style="font-family: sans-serif"> <h1>Rendering Error Occurred</h1>')
          message.append("<p>${result.cause().message}</p>")
          if (Environment.isDev()) {
            message.append('<ul style="list-style: none">')
            result.cause().stackTrace.each { trace ->
              message.append("<li>${trace}</li>")
            }
            message.append("</ul>")
          }
          logger.error(result.cause().message)
          message.append("</div>")
          putHeader("content-length", String.valueOf(message.toString().length()))
              .write(message.toString()).end()
          context.fail(HTTP_INTERNAL_ERROR)
        }
      })
    } else {
      error().text("No template engine defined.")
      context.fail(HTTP_INTERNAL_ERROR)
    };
  }

  /**
   * Render a JSON response.
   * @param bean A POJO that can be serialized by Jackson.
   */
  void json(Object bean) {
    String jsonString = new ObjectMapper().writeValueAsString(bean)
    json(jsonString)
  }

  /**
   * Render a JSON response.
   * @param body A well-formed JSON string.
   */
  void json(String body) {
    putHeader("content-type", "application/json").end(body);
  }

  ApexHttpServerResponse ok() {
    setStatusCode(HTTP_OK)
    return this
  }

  void ok(String message) {
    ok().text(message)
  }

  /**
   * Redirect the user to another URL. Different from the {@link RoutingContext#reroute} method.
   * @param location The URL/path to redirect to
   */
  void redirect(String location) {
    setStatusCode(HTTP_MOVED_TEMP).putHeader("location", location)
  }

  /**
   * Render a plain text response.
   * @param text Text to be rendered.
   */
  void text(String text) {
    putHeader("Content-Type", "text/plain").end(text)
  }
}
