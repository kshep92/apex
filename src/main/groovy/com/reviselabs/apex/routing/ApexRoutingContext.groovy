package com.reviselabs.apex.routing
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.inject.Inject
import com.google.inject.name.Named
import com.reviselabs.apex.contracts.ApplicationContextContainer
import com.reviselabs.apex.di.DependencyManager
import com.reviselabs.validation.Form
import io.vertx.core.Vertx
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.templ.TemplateEngine
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.annotation.Nullable
/**
 * Author: Kevin Sheppard <kevin@reviselabs.com>
 * Created: 04/03/2017
 *
 * Trimmed down version of {@link RoutingContext} for use
 * in {@link ApexHttpServerResponse}
 * Provides a few nifty convenience methods.
 */
class ApexRoutingContext implements ApplicationContextContainer {

  @Delegate
  RoutingContext context
  TemplateEngine templateEngine
  String templatesPath
  Logger logger
  Vertx vertx

  @Inject
  public ApexRoutingContext(Vertx vertx, @Nullable TemplateEngine templateEngine, @Named("templatesPath") String templatesPath) {
    this.templateEngine = templateEngine
    this.templatesPath = templatesPath
    this.vertx = vertx
    logger = LoggerFactory.getLogger(getClass())
  }

  void expireCookie(String name) {
    getCookie(name).setMaxAge(0)
    removeCookie(name) // Not sure if this next step is necessary
  }

  /**
   * Get the resqest body as a {@link com.reviselabs.validation.Form} object for validation purposes.
   * Only works with JSON bodies right now.
   * @param type
   * @return
   */
  public <T> Form<T> getBodyAsForm(Class<T> type) {
    logger.warn("getBodyAsForm<T>: This only works for JSON body types at the moment. Will fix in subsequent release. Maybe.")
    try {
      //TODO: Cater for application/x-www-form-urlencoded
      def instance = new ObjectMapper().readValue(bodyAsString, type)
      return Form.form(type).bind(instance);
    } catch (JsonProcessingException e) {
      logger.error(e.message)
      return Form.form(type).bind(type.newInstance())
    }
  }

  @Override
  public <T> T getInstance(Class<T> instanceType) {
    return DependencyManager.injector.getInstance(instanceType)
  }

  ApexHttpServerResponse response() {
    return new ApexHttpServerResponse(this, context.response(), templateEngine, templatesPath)
  }

}
