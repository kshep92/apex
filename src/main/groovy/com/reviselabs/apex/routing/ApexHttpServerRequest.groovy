package com.reviselabs.apex.routing

import io.vertx.core.http.HttpServerRequest
import io.vertx.ext.web.RoutingContext
import org.slf4j.Logger
import org.slf4j.LoggerFactory
/**
 * Author: Kevin Sheppard <kevin@reviselabs.com>
 * Created: 3/4/2017
 *
 * Remnant of a previous API reworking. Will leave here in case we decide to add
 * some utility methods.
 *
 */
class ApexHttpServerRequest {
  @Delegate
  HttpServerRequest request
  private RoutingContext context
  Logger logger

  public ApexHttpServerRequest() {
    logger = LoggerFactory.getLogger(getClass())
  }

  void setContext(RoutingContext context) {
    this.context = context
  }
}
