package com.reviselabs.apex.contracts;

/**
 * Author: Kevin Sheppard <kevin@reviselabs.com>
 * Created: 04/03/2017
 *
 * An interface for use by all library-wide components that want to make use of
 * dependency injection.
 */
public interface ApplicationContextContainer {
  <T> T getInstance(Class<T> clazz);
}
