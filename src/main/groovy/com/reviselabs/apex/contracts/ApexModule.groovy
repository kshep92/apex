package com.reviselabs.apex.contracts

/**
 * Author: Kevin Sheppard <kevin@reviselabs.com>
 * Created: 04/03/2017
 *
 * An interface all developers should use when creating either an
 * {@link com.reviselabs.apex.ApexApplication} or a {@link com.reviselabs.apex.routing.SubRouter}.
 */
interface ApexModule {
  void configure()
}