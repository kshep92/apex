package com.reviselabs.apex.config

import com.google.inject.AbstractModule
import com.google.inject.Provides
import com.google.inject.Singleton
import com.google.inject.name.Named
import io.vertx.core.Vertx
import io.vertx.ext.web.templ.TemplateEngine

/**
 * Author: Kevin Sheppard <kevin@reviselabs.com>
 * Created: 04/03/2017
 *
 * Base configuration module for a typical Apex application.
 *
 * Provides:
 * <ul>
 *   <li>A singleton Vertx instance.</li>
 *   <li>An instance of a {@link TemplateEngine}.</li>
 *   <li>A global pointer to where you keep your templates via the templatePath field.</li>
 * </ul>
 */

class BaseConfiguration extends AbstractModule {
  Vertx vertx
  TemplateEngine templateEngine
  String templatesPath

  @Override
  protected void configure() {

  }

  void setVertx(Vertx vertx) {
    this.vertx = vertx
  }

  void setTemplateEngine(TemplateEngine templateEngine) {
    this.templateEngine = templateEngine
  }

  void setTemplatesPath(String templatesPath) {
    this.templatesPath = templatesPath
  }

  @Provides
  @Singleton
  public Vertx getVertx() {
    return vertx;
  }

  @Provides
  public TemplateEngine getTemplateEngine() {
    return templateEngine;
  }

  @Provides
  @Named("templatesPath")
  String getTemplatesPath() {
    return templatesPath
  }
}
