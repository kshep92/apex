package com.reviselabs.apex.config;

/**
 * Author: Kevin Sheppard <kevin@reviselabs.com>
 * Created: 04/03/2017
 *
 * A utility class for determining whether the application is running in a
 * production or development environment.
 */
public class Environment {
  /**
   * Check if the app is running in production.
   * @return true/false
   */
  public static Boolean isProd() {
    return getEnv().equals("production");
  }

  /**
   * Check if the app is running in a dev environment.
   * @return true/false
   */
  public static Boolean isDev() {
    return getEnv().equals("development");
  }

  /**
   * Get the environment the app is running in.
   * @return Either "production" or "development"
   */
  private static String getEnv() {
    return (System.getProperty("apex.env") == null) ? "development" : "production";
  }
}
