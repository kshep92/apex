import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by Kevin on 3/4/2017.
 *
 * Test class to ensure Before filters are working
 */
@RunWith(VertxUnitRunner)
class InterceptorsTest extends AppTestSuite {

  @Test
  void specificBeforeFilterTest(TestContext context) {
    promise(context)
    client.get('/admin/me', {res ->
      context.assertEquals(200, res.statusCode())
      res.bodyHandler({body ->
        context.assertEquals("true", body.toString())
        resolve()
      })
    }).end()
  }

  @Test
  void generalBeforeFilterTest(TestContext context) {
    promise(context)
    client.get('/me', {res ->
      context.assertEquals(200, res.statusCode())
      res.bodyHandler({body ->
        context.assertEquals("kevin@mail.com", body.toString())
        resolve()
      })
    }).end()
  }

}
