import com.reviselabs.apex.ApexApplication;
import io.vertx.ext.web.templ.JadeTemplateEngine;

import java.util.HashMap;
import java.util.Map;

/**
 * Author: Kevin Sheppard <kevin@reviselabs.com>
 * Created: 3/5/2017
 *
 * Just made to prove that Apex works for Java as well
 */
public class ExampleJavaApplication extends ApexApplication {
  @Override
  public void configure() {
    serverOptions.setPort(9000);
    templateEngine = JadeTemplateEngine.create().setExtension(".jade");
    templatesPath = "src/test/files/templates";

    get("/", context -> {
      Map<String, Object> data = new HashMap<>();
      data.put("greeting", "Hello, world!");
      context.response().render("greeting", data);
    });
  }

  public static void main(String[] args) {
    new ExampleJavaApplication().start();
  }
}
