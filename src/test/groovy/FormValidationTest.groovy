import groovy.json.JsonOutput
import io.vertx.core.json.JsonArray
import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import org.junit.Test
import org.junit.runner.RunWith
/**
 * Created by Kevin on 3/4/2017.
 *
 * Test class to ensure form validation works
 */
@RunWith(VertxUnitRunner)
class FormValidationTest extends AppTestSuite {

  @Test
  void validSubmissionTest(TestContext context) {
    String json = JsonOutput.toJson([email: 'kevin@mail.com', name: 'Kevin Sheppard'])
    promise(context)
    client.post('/users', { response ->
      context.assertEquals(200, response.statusCode())
      response.bodyHandler({body ->
        context.assertTrue(body.toString().contains('Kevin Sheppard, kevin@mail.com'))
        resolve()
      })
    })
        .putHeader('content-type', 'application/json')
        .putHeader('content-length', String.valueOf(json.length()))
        .write(json)
        .end()

  }

  @Test
  void invalidSubmissionTest(TestContext context) {
    String json = JsonOutput.toJson([email: 'kevin@mail.com'])
    promise(context)
    client.post('/users', { response ->
      context.assertEquals(400, response.statusCode())
      response.bodyHandler({body ->
        String missingField = new JsonArray(body.toString()).getJsonObject(0).getString("field")
        context.assertEquals('name', missingField)
        resolve()
      })
    })
        .putHeader('content-type', 'application/json')
        .end(json)
  }

}
