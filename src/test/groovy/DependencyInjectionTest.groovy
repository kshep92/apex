import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by Kevin on 3/4/2017.
 *
 * Test cases for Dependency Injection
 */
@RunWith(VertxUnitRunner)
class DependencyInjectionTest extends AppTestSuite {

  @Test
  void databaseInjectionTest(TestContext context) {
    promise(context)
    client.get('/db', { res ->
      context.assertEquals(200, res.statusCode())
      res.bodyHandler({body ->
        context.assertTrue(body.toString().contains("localhost, root, root"))
        resolve()
      })
    }).end()
  }
}
