import com.reviselabs.apex.ApexApplication
import com.reviselabs.validation.Form
import config.TestConfig
import data.DAO
import io.vertx.ext.web.templ.JadeTemplateEngine
import models.User
import routes.AdminRoutes

public class ExampleApplication extends ApexApplication {

  @Override
  void configure() {

    templateEngine = JadeTemplateEngine.create().setExtension('.jade')

    templatesPath = 'src/test/files/templates'

    enableForms()

    enableCookies()


    staticFiles('/assets/*', 'src/test/files')

    before('/admin/me', { ctx ->
      ctx.put('admin', true)
      ctx.next()
    })

    before({ ctx ->
      ctx.request().headers().add("new-header", "the-value")
      ctx.put('user', 'kevin@mail.com')
      ctx.next()
    })

    register(new TestConfig())

    get('/', { ctx -> ctx.response().ok('OK') })

    post("/", { ctx ->
      ctx.response().ok(ctx.bodyAsString)
    });

    put("/", { ctx ->
      ctx.response().ok(ctx.bodyAsString)
    });

    delete("/", { ctx -> ctx.response().ok('DELETED') });

    get('/greeting', { ctx ->
      ctx.response().render("greeting", [greeting: 'Hello, world!' ]);
    })

    mount('/admin', AdminRoutes)

    get('/db', { ctx ->
      String db = ctx.getInstance(DAO).database.toString()
      ctx.response().ok(db)
    })

    get('/me', { ctx ->
      ctx.response().ok(ctx.get('user').toString())
    })

    post('/users', { ctx ->
      Form<User> userForm = ctx.getBodyAsForm(User)
      if(userForm.valid())
        ctx.response().ok(userForm.get().toString())
      else
        ctx.response().badRequest().json(userForm.errorsAsJson)
    })

  }

  static void main(String[] args) {
    new ExampleApplication().start()
  }
}
