package config

import com.google.inject.AbstractModule
import com.google.inject.Provides
import com.google.inject.Singleton
import data.Database

/**
 * Created by Kevin on 3/4/2017.
 *
 * An AbstractModule for testing dependency injection.
 */
class TestConfig extends AbstractModule {

  @Override
  protected void configure() {

  }

  @SuppressWarnings("GrMethodMayBeStatic")
  @Provides
  @Singleton
  Database getDatabase() {
    Database database = new Database(url: 'localhost', user: 'root', password: 'root')
    database
  }
}
