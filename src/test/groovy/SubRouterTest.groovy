import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by Kevin on 3/4/2017.
 *
 * Tests to ensure sub-routers are working properly.
 */
@RunWith(VertxUnitRunner)
class SubRouterTest extends AppTestSuite {

  @Test
  void subRouteTest(TestContext context) {
    promise(context)
    client.get("/admin").handler({ response ->
      context.assertEquals(200, response.statusCode())
      response.bodyHandler({body ->
        context.assertEquals("/admin", body.toString())
        resolve()
      })
    }).end()
  }

}
