package data

import groovy.transform.ToString

/**
 * Created by Kevin on 12/23/2016.
 *
 * Dummy Database class for testing dependency injection.
 */
@ToString
class Database {
  String url
  String user
  String password
}
